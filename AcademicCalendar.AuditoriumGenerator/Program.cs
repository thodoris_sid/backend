﻿using AcademicCalendar.Core.Entities;
using AcademicCalendar.Infrastructure.Data;
using System;

namespace AcademicCalendar.AuditoriumGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new AcademicCalendarDbContext();
            CreateTwenty(context);
        }

        private static void CreateTwenty(AcademicCalendarDbContext context)
        {
            var newAuditorium = new Auditorium();
            newAuditorium.Capacity = 30;
            context.Auditorium.Add(newAuditorium);
        }
    }
}
