﻿using AcademicCalendar.Controllers.Lecture.Models;
using AcademicCalendar.Core.Entities;
using AcademicCalendar.Infrastructure.Interfaces;
using AcademicCalendar.Infrastructure.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace AcademicCalendar
{
    [RoutePrefix("api/lectures")]
    public class LectureController : ApiController
    {

        private readonly ILectureService _lecture;
        private readonly IMapper _mapper;
        private readonly ICourseService _course;
        private readonly IAuditoriumService _auditorium;

        public LectureController(ILectureService lecture, IMapper mapper, ICourseService course, IAuditoriumService auditorium)
        {
            _lecture = lecture;
            _mapper = mapper;
            _course = course;
            _auditorium = auditorium;
        }

        [HttpPost]
        [Route("add")]
        public async Task<IHttpActionResult> AddLectureForCourse(LectureBM model)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var course = await _course.GetCourse(model.CourseId);

                if (course == null)
                {
                    return BadRequest("Course with this id does not exist");
                }                
                
                var auditorium = await _auditorium.GetAuditorium(model.AuditoriumId);

                if (auditorium == null)
                {
                    return BadRequest("Auditorium with this id does not exist");
                }

                course.IsInCalendar = true;

                var lectureDto = _mapper.Map<LectureDto>(model);
                await _lecture.AddLecture(lectureDto);

                return Ok(lectureDto);

            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [Route("delete/{courseId}")]
        public async Task<IHttpActionResult> DeleteLecture(int courseId)
        {
            try
            {
                await _lecture.DeleteLecture(courseId);

                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [Route("delete")]
        public async Task<IHttpActionResult> DeleteLectures()
        {
            try
            {
                await _lecture.DeleteLectures();

                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("studiesType/{studiesType}")]
        public async Task<IHttpActionResult> GetLectures(int studiesType)
        {
            List<LectureDto> lecturesDto = await _lecture.GetAllLecturesByStudiesType(studiesType);
            return Ok(lecturesDto);
        }

        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> GetAllLectures()
        {
            List<LectureDto> lecturesDto = await _lecture.GetAllLectures();
            return Ok(lecturesDto);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IHttpActionResult> GetLecture(int id)
        {
            var lecture = await _lecture.GetLecture(id);
            var lectureDto = _mapper.Map<Lecture, LectureDto>(lecture);
            return Ok(lectureDto);
        }
    }
}