﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AcademicCalendar.Controllers.Lecture.Models
{
    public class LectureBM
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int CourseId { get; set; }
        public int AuditoriumId { get; set; }
    }
}