﻿using AcademicCalendar.Controllers.Auditorium.Models;
using AcademicCalendar.Core.Entities;
using AcademicCalendar.Infrastructure.Interfaces;
using AcademicCalendar.Infrastructure.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace AcademicCalendar
{
    [RoutePrefix("api/auditoriums")]
    //[Authorize]
    public class AuditoriumController : ApiController
    {
        private readonly IAuditoriumService _auditorium;
        private readonly IMapper _mapper;

        public AuditoriumController(IAuditoriumService auditorium, IMapper mapper)
        {

            _auditorium = auditorium;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("add")]
        public async Task<IHttpActionResult> AddAuditorium(AuditoriumBM model)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }


                var auditoriumDto = _mapper.Map<AuditoriumDto>(model);
                await _auditorium.AddAuditorium(auditoriumDto);

                return Ok(auditoriumDto);

            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> GetAuditoriums()
        {
            List<Auditorium> auditoriums = await _auditorium.GetAllAuditoriums();
            var auditoriumsDto = _mapper.Map<List<Auditorium>, List<AuditoriumDto>>(auditoriums);
            return Ok(auditoriumsDto);
        }

        [HttpPost]
        [Route("{id}/update")]
        public async Task<IHttpActionResult> UpdateAuditorium(AuditoriumBM model, int id)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var auditorium = await _auditorium.GetAuditorium(id);
                if (auditorium == null)
                {
                    return NotFound();
                }

                var auditoriumDto = _mapper.Map<AuditoriumDto>(model);
                await _auditorium.UpdateAuditorium(auditorium, auditoriumDto);

                return Ok(auditoriumDto);

            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IHttpActionResult> GetAuditorium(int id)
        {
            var auditorium = await _auditorium.GetAuditorium(id);
            if (auditorium == null)
            {
                return NotFound();
            }
            var auditoriumDto = _mapper.Map<Auditorium, AuditoriumDto>(auditorium);
            return Ok(auditoriumDto);
        }

        [HttpPost]
        [Route("delete/{id}")]
        public async Task<IHttpActionResult> DeleteAuditorium(int id)
        {
            var auditorium = await _auditorium.GetAuditorium(id);
            if (auditorium == null)
            {
                return NotFound();
            }
            await _auditorium.DeleteAuditorium(auditorium);
            return Ok();
        }
    }
}