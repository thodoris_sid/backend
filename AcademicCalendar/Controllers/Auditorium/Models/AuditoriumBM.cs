﻿using AcademicCalendar.Controllers.Equipment.Models;
using AcademicCalendar.Core.Enumerations;
using AcademicCalendar.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AcademicCalendar.Controllers.Auditorium.Models
{
    public class AuditoriumBM
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public int Capacity { get; set; }
        [Required]
        public AuditoriumType Type { get; set; }
        [Required]
        public ICollection<EquipmentBM> Equipment { get; set; }
    }
}