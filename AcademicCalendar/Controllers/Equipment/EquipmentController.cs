﻿using AcademicCalendar.Controllers.Equipment.Models;
using AcademicCalendar.Core.Entities;
using AcademicCalendar.Infrastructure.Interfaces;
using AcademicCalendar.Infrastructure.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace AcademicCalendar
{
    [RoutePrefix("api/equipment")]
    public class EquipmentController : ApiController
    {
        private readonly IMapper _mapper;
        private readonly IEquipmentService _equipment;

        public EquipmentController(IMapper mapper, IEquipmentService equipment)
        {
            _mapper = mapper;
            _equipment = equipment;
        }

        [HttpPost]
        [Route("add")]
        public async Task<IHttpActionResult> AddEquipment(EquipmentBM model)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var equipmentDto = _mapper.Map<EquipmentDto>(model);
                await _equipment.AddEquipment(equipmentDto);

                return Ok(equipmentDto);

            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> GetEquipment()
        {
            var courses = await _equipment.GetAllEquipment();
            return Ok(courses);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IHttpActionResult> GetEquipment(int id)
        {
            var equipment = await _equipment.GetEquipment(id);
            if (equipment == null)
            {
                return NotFound();
            }
            var equipmentDto = _mapper.Map<Equipment, EquipmentDto>(equipment);
            return Ok(equipmentDto);
        }

    }
}