﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AcademicCalendar.Controllers.Equipment.Models
{
    public class EquipmentBM
    {
        [Required]
        public string Type { get; set; }
        [Required]
        public int TypeCount { get; set; }

    }
}