﻿using AcademicCalendar.Controllers.Equipment.Models;
using AcademicCalendar.Core.Entities;
using AcademicCalendar.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AcademicCalendar.Controllers.Course.Models
{
    public class CourseBM
    {
        [Required]
        public string CourseCode { get; set; }
        [Required]
        public int Semester { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        public int ProfessorId { get; set; }
        [Required]
        public bool IsObligatory { get; set; }
        [Required]
        public ICollection<EquipmentBM> Equipment { get; set; }
        [Required]
        public int StudiesId { get; set; }
    }
}