﻿using AcademicCalendar.Controllers.Course.Models;
using AcademicCalendar.Core.Entities;
using AcademicCalendar.Infrastructure.Interfaces;
using AcademicCalendar.Infrastructure.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AcademicCalendar.CourseController
{
    [RoutePrefix("api/courses")]
    public class CourseController : ApiController
    {
        private readonly IMapper _mapper;
        private readonly ICourseService _course;
        private readonly IProfessorService _professor;

        public CourseController(IMapper mapper, ICourseService course, IProfessorService professor)
        {
            _mapper = mapper;
            _course = course;
            _professor = professor;
        }

        [HttpPost]
        [Route("add")]
        public async Task<IHttpActionResult> AddCourse(CourseBM model)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var professorExists = await _professor.GetProfessor(model.ProfessorId);

                if (professorExists == null)
                {
                    return BadRequest("Professor with this id does not exist");
                }

                var courseDto = _mapper.Map<CourseDto>(model);
                await _course.AddCourse(courseDto);

                return Ok(courseDto);

            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


        [HttpGet]
        [Route("studiesType/{studiesType}")]
        public async Task<IHttpActionResult> GetCourses(int studiesType)
        {
            var courses = await _course.GetAllCourses(studiesType);
            var coursesDto = _mapper.Map<List<CourseDto>>(courses);
            return Ok(coursesDto);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IHttpActionResult> GetCourse(int id)
        {
            var course = await _course.GetCourse(id);
            if (course == null)
            {
                return NotFound();
            }
            var courseDto = _mapper.Map<Course, CourseDto>(course);
            return Ok(courseDto);
        }

        [HttpPost]
        [Route("delete/{id}")]
        public async Task<IHttpActionResult> DeleteCourse(int id)
        {
            var course = await _course.GetCourse(id);
            if (course == null)
            {
                return NotFound();
            }
            await _course.DeleteCourse(course);
            return Ok();
        }


        [HttpGet]
        [Route("semester/{semesterId}")]
        public async Task<IHttpActionResult> GetCoursesFromSemester(int semesterId)
        {
            var courses = await _course.GetCoursesFromSemester(semesterId);
            //var coursesDto = _mapper.Map<List<CourseDto>>(courses);
            return Ok(courses);
        }

    }
}
