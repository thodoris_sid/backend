﻿using AcademicCalendar.Controllers.Professor;
using AcademicCalendar.Core.Entities;
using AcademicCalendar.Infrastructure.Interfaces;
using AcademicCalendar.Infrastructure.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace AcademicCalendar
{
    [RoutePrefix("api/professors")]
    public class ProfessorController : ApiController
    {

        private readonly IProfessorService _professor;
        private readonly IMapper _mapper;

        public ProfessorController(IProfessorService professor, IMapper mapper)
        {

            _professor = professor;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("add")]
        public async Task<IHttpActionResult> AddProfessor(ProfessorBM model)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }


                var professorDto = _mapper.Map<ProfessorDto>(model);
                await _professor.AddProfessor(professorDto);

                return Ok(professorDto);

            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> GetProfessors(bool includeCourses = false)
        {
            List<Professor> professors = await _professor.GetAllProfessors(includeCourses);
            var professorDto = _mapper.Map<List<Professor>,List<ProfessorDto>>(professors);
            return Ok(professorDto);
        }

        [HttpPost]
        [Route("{id}/update")]
        public async Task<IHttpActionResult> UpdateProfessor(ProfessorBM model, int id)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var professor = await _professor.GetProfessor(id);
                if (professor == null)
                {
                    return NotFound();
                }

                var professorDto = _mapper.Map<ProfessorDto>(model);
                await _professor.UpdateProfessor(professor, professorDto);

                return Ok(professorDto);

            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IHttpActionResult> GetProfessor(int id)
        {
            var professor = await _professor.GetProfessor(id);
            if (professor == null)
            {
                return NotFound();
            }
            var professorDto = _mapper.Map<Professor, ProfessorDto>(professor);
            return Ok(professorDto);
        }

        [HttpPost]
        [Route("delete/{id}")]
        public async Task<IHttpActionResult> DeleteProfessor(int id)
        {
            var professor = await _professor.GetProfessor(id);
            if (professor == null)
            {
                return NotFound();
            }
            await _professor.DeleteProfessor(professor);
            return Ok();
        }

    }
}