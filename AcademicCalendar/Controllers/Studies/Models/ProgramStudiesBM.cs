﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AcademicCalendar.Controllers.Studies.Models
{
    public class ProgramStudiesBM
    {
            [Required]
            public string Name { get; set; }
            [Required]
            public string UrlParameter { get; set; }

    }
}