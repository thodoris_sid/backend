﻿using AcademicCalendar.Infrastructure.Interfaces;
using AcademicCalendar.Core.Entities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using AcademicCalendar.Controllers.Studies.Models;
using AcademicCalendar.Infrastructure.Models;

namespace AcademicCalendar
{
    [RoutePrefix("api/programStudies")]
    public class ProgramStudiesController : ApiController
    {

        private readonly IProgramStudiesService _studies;
        private readonly IMapper _mapper;

        public ProgramStudiesController(IProgramStudiesService studies, IMapper mapper)
        {

            _studies = studies;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> GetProgramStudies()
        {
            List<Studies> programStudies = await _studies.GetProgramStudies();
            return Ok(programStudies);
        }

        [HttpPost]
        [Route("add")]
        public async Task<IHttpActionResult> AddProgramStudies(ProgramStudiesBM model)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var programStudiesDto = _mapper.Map<ProgramStudiesDto>(model);
                await _studies.AddProgramStudies(programStudiesDto);

                return Ok();

            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [Route("delete/{id}")]
        public async Task<IHttpActionResult> DeleteProgramStudies(int id)
        {
            var programStudies = await _studies.GetProgramStudies(id);
            if (programStudies == null)
            {
                return NotFound();
            }
            await _studies.DeleteProgramStudies(programStudies);
            return Ok();
        }

    }
}