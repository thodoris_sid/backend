﻿using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;
using System.Threading.Tasks;
using System.Web.Cors;

[assembly: OwinStartup(typeof(AcademicCalendar.Startup))]

namespace AcademicCalendar
{
    public partial class Startup
    {
        public void ConfigurationCors(IAppBuilder app)
        {
            var corsPolicy = new CorsPolicy
            {
                AllowAnyMethod = true,
                AllowAnyHeader = true
            };
            corsPolicy.ExposedHeaders.Add("X-Pagination");
            corsPolicy.ExposedHeaders.Add("Content-Disposition");

            corsPolicy.Origins.Add("http://localhost:4200");
            corsPolicy.Origins.Add("http://localhost:4200/");
            var corsOptions = new CorsOptions
            {
                PolicyProvider = new CorsPolicyProvider
                {
                    PolicyResolver = context => Task.FromResult(corsPolicy)
                }
            };

            app.UseCors(corsOptions);
        }
    }
}