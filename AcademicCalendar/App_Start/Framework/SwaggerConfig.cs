using System.Web.Http;
using WebActivatorEx;
using AcademicCalendar;
using Swashbuckle.Application;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace AcademicCalendar
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                    {
                        c.SingleApiVersion("v1", "AcademicCalendar");
                    })
                .EnableSwaggerUi(c =>
                    {
                    });
        }
    }
}
