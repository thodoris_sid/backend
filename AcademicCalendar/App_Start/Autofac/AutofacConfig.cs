﻿using AcademicCalendar.Infrastructure.Data;
using AcademicCalendar.Infrastructure.Interfaces;
using AcademicCalendar.Infrastructure.Services;
using AcademicCalendar.Mapper;
using Autofac;
using Autofac.Integration.WebApi;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace AcademicCalendar.App_Start.Autofac
{
    public class AutofacConfig
    {
        public static void Register()
        {
            var bldr = new ContainerBuilder();
            var config = GlobalConfiguration.Configuration;
            bldr.RegisterApiControllers(Assembly.GetExecutingAssembly());
            RegisterServices(bldr);
            //bldr.RegisterControllers(typeof(WebApiApplication).Assembly);
            bldr.RegisterWebApiFilterProvider(config);
            bldr.RegisterWebApiModelBinderProvider();
            var container = bldr.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static void RegisterServices(ContainerBuilder bldr)
        {

            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            });

            // Automapper
            bldr.RegisterInstance(config.CreateMapper()).As<IMapper>().SingleInstance();

            // DbContext
            bldr.RegisterType<AcademicCalendarDbContext>().InstancePerRequest();

            // Services
            bldr.RegisterType<CourseService>().As<ICourseService>().InstancePerRequest();
            bldr.RegisterType<ProfessorService>().As<IProfessorService>().InstancePerRequest();
            bldr.RegisterType<EquipmentService>().As<IEquipmentService>().InstancePerRequest();
            bldr.RegisterType<AuditoriumService>().As<IAuditoriumService>().InstancePerRequest();
            bldr.RegisterType<LectureService>().As<ILectureService>().InstancePerRequest();
            bldr.RegisterType<ProgramStudiesService>().As<IProgramStudiesService>().InstancePerRequest();

        }
    }
}