﻿using AcademicCalendar.Controllers.Auditorium.Models;
using AcademicCalendar.Controllers.Course.Models;
using AcademicCalendar.Controllers.Equipment.Models;
using AcademicCalendar.Controllers.Lecture.Models;
using AcademicCalendar.Controllers.Professor;
using AcademicCalendar.Controllers.Studies.Models;
using AcademicCalendar.Core.Entities;
using AcademicCalendar.Infrastructure.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AcademicCalendar.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Auditorium, AuditoriumDto>()
                .ReverseMap();

            CreateMap<AuditoriumBM, AuditoriumDto>();

            CreateMap<List<Auditorium>, AuditoriumBM>()
                .ReverseMap();

            CreateMap<Course, CourseDto>()
                .ReverseMap();

            CreateMap<CourseBM, CourseDto>();

            CreateMap<List<Course>, CourseBM>()
                .ReverseMap();

            CreateMap<Equipment, EquipmentDto>()
                .ReverseMap();

            CreateMap<EquipmentBM, EquipmentDto>();

            CreateMap<List<Equipment>, EquipmentBM>()
                .ReverseMap();

            CreateMap<Professor, ProfessorDto>()
                .ReverseMap();

            CreateMap<ProfessorBM, ProfessorDto>();

            CreateMap<List<Professor>, ProfessorBM>()
                .ReverseMap();

            CreateMap<Studies, ProgramStudiesDto>()
                .ReverseMap();

            CreateMap<ProgramStudiesBM, ProgramStudiesDto>();

            CreateMap<List<Studies>, ProgramStudiesBM>()
                .ReverseMap();

            CreateMap<Lecture, LectureDto>()
                .ReverseMap();

            CreateMap<LectureBM, LectureDto>();

            CreateMap<List<Lecture>, LectureBM>()
                .ReverseMap();

        }
    }
}