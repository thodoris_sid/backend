﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(AcademicCalendar.Startup))]

namespace AcademicCalendar
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigurationCors(app);
            ConfigureAuth(app);
        }
    }
}
