﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Infrastructure.Exceptions
{
    public class CourseException : Exception
    {
        public CourseException()
        {

        }

        public CourseException(string message) : base(message)
        {

        }
    }


    public class SameCourse : CourseException
    {
        public SameCourse()
        {

        }

        public SameCourse(string message) : base(message)
        {

        }
    }
}
