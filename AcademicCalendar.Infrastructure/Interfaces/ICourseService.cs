﻿using AcademicCalendar.Core.Entities;
using AcademicCalendar.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Infrastructure.Interfaces
{
    public interface ICourseService
    {
        Task AddCourse(CourseDto course);
        Task DeleteCourse(Course course);
        Task<List<Course>> GetAllCourses(int studiesType);
        Task<List<CourseDto>> GetCoursesFromSemester(int semesterId);
        Task<Course> GetCourse(int id);


    }
}
