﻿using AcademicCalendar.Core.Entities;
using AcademicCalendar.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Infrastructure.Interfaces
{
    public interface IAuditoriumService
    {
        Task AddAuditorium(AuditoriumDto auditorium);
        Task UpdateAuditorium(Auditorium auditorium, AuditoriumDto auditoriumDto);
        Task<List<Auditorium>> GetAllAuditoriums();
        Task<Auditorium> GetAuditorium(int id);
        Task DeleteAuditorium(Auditorium auditorium);
    }
}
