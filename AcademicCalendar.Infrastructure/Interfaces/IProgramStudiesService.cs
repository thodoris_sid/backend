﻿using AcademicCalendar.Core.Entities;
using AcademicCalendar.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Infrastructure.Interfaces
{
    public interface IProgramStudiesService
    {
        Task AddProgramStudies(ProgramStudiesDto programStudies);
        Task<List<Studies>> GetProgramStudies();
        Task<Studies> GetProgramStudies(int id);
        Task DeleteProgramStudies(Studies studies);
    }
}
