﻿using AcademicCalendar.Core.Entities;
using AcademicCalendar.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Infrastructure.Interfaces
{
    public interface IEquipmentService
    {
        Task AddEquipment(EquipmentDto equipment);
        Task<List<EquipmentDto>> GetAllEquipment();
        Task<Equipment> GetEquipment(int id);
    }
}
