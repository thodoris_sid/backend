﻿using AcademicCalendar.Core.Entities;
using AcademicCalendar.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Infrastructure.Interfaces
{
    public interface ILectureService
    {
        Task AddLecture(LectureDto lecture);
        Task DeleteLecture(int courseId);
        Task DeleteLectures();
        Task<List<LectureDto>> GetAllLecturesByStudiesType(int studiesType);
        Task<List<LectureDto>> GetAllLectures();
        Task<Lecture> GetLecture(int id);
    }
}
