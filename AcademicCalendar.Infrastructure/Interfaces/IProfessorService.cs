﻿using AcademicCalendar.Core.Entities;
using AcademicCalendar.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Infrastructure.Interfaces
{
    public interface IProfessorService
    {
        Task AddProfessor(ProfessorDto professor);
        Task UpdateProfessor(Professor professor, ProfessorDto professorDto);
        Task<List<Professor>> GetAllProfessors(bool includeCourses = false);
        Task<Professor> GetProfessor(int id);
        Task DeleteProfessor(Professor professor);
    }
}
