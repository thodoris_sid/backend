namespace AcademicCalendar.Infrastructure.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Auditoriums",
                c => new
                    {
                        AuditoriumId = c.Int(nullable: false, identity: true),
                        Capacity = c.Int(nullable: false),
                        Audit_CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Audit_UpdatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ExtraData = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.AuditoriumId);
            
            CreateTable(
                "dbo.Lectures",
                c => new
                    {
                        LectureId = c.Int(nullable: false, identity: true),
                        StartTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        EndTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CourseId = c.Int(nullable: false),
                        AuditoriumId = c.Int(nullable: false),
                        Audit_CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Audit_UpdatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ExtraData = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.LectureId)
                .ForeignKey("dbo.Auditoriums", t => t.AuditoriumId, cascadeDelete: true)
                .ForeignKey("dbo.Courses", t => t.CourseId, cascadeDelete: true)
                .Index(t => t.CourseId)
                .Index(t => t.AuditoriumId);
            
            CreateTable(
                "dbo.Courses",
                c => new
                    {
                        CourseId = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        ProfessorId = c.Int(nullable: false),
                        Audit_CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Audit_UpdatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ExtraData = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.CourseId)
                .ForeignKey("dbo.Professors", t => t.ProfessorId, cascadeDelete: true)
                .Index(t => t.ProfessorId);
            
            CreateTable(
                "dbo.Professors",
                c => new
                    {
                        ProfessorId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Office = c.String(),
                        Phone = c.String(),
                        Audit_CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Audit_UpdatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ExtraData = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.ProfessorId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Lectures", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.Courses", "ProfessorId", "dbo.Professors");
            DropForeignKey("dbo.Lectures", "AuditoriumId", "dbo.Auditoriums");
            DropIndex("dbo.Courses", new[] { "ProfessorId" });
            DropIndex("dbo.Lectures", new[] { "AuditoriumId" });
            DropIndex("dbo.Lectures", new[] { "CourseId" });
            DropTable("dbo.Professors");
            DropTable("dbo.Courses");
            DropTable("dbo.Lectures");
            DropTable("dbo.Auditoriums");
        }
    }
}
