namespace AcademicCalendar.Infrastructure.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCourseField : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Courses", "IsInCalendar", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Courses", "IsInCalendar");
        }
    }
}
