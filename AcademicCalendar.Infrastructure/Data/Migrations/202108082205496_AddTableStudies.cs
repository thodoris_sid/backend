namespace AcademicCalendar.Infrastructure.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTableStudies : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Studies",
                c => new
                    {
                        StudiesId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Audit_CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Audit_UpdatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ExtraData = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.StudiesId);
            
            AddColumn("dbo.Courses", "StudiesId", c => c.Int());
            CreateIndex("dbo.Courses", "StudiesId");
            AddForeignKey("dbo.Courses", "StudiesId", "dbo.Studies", "StudiesId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Courses", "StudiesId", "dbo.Studies");
            DropIndex("dbo.Courses", new[] { "StudiesId" });
            DropColumn("dbo.Courses", "StudiesId");
            DropTable("dbo.Studies");
        }
    }
}
