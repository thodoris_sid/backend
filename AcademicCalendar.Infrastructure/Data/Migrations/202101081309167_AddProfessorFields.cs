namespace AcademicCalendar.Infrastructure.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProfessorFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Professors", "Email", c => c.String());
            AddColumn("dbo.Professors", "Website", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Professors", "Website");
            DropColumn("dbo.Professors", "Email");
        }
    }
}
