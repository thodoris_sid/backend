namespace AcademicCalendar.Infrastructure.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCourseFieldIsObligatory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Courses", "IsObligatory", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Courses", "IsObligatory");
        }
    }
}
