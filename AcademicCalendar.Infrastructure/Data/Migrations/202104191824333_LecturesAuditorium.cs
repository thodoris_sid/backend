namespace AcademicCalendar.Infrastructure.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LecturesAuditorium : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Lectures", "Auditorium_AuditoriumId", "dbo.Auditoriums");
            DropIndex("dbo.Lectures", new[] { "Auditorium_AuditoriumId" });
            RenameColumn(table: "dbo.Lectures", name: "Auditorium_AuditoriumId", newName: "AuditoriumId");
            AlterColumn("dbo.Lectures", "AuditoriumId", c => c.Int(nullable: true));
            CreateIndex("dbo.Lectures", "AuditoriumId");
            AddForeignKey("dbo.Lectures", "AuditoriumId", "dbo.Auditoriums", "AuditoriumId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Lectures", "AuditoriumId", "dbo.Auditoriums");
            DropIndex("dbo.Lectures", new[] { "AuditoriumId" });
            AlterColumn("dbo.Lectures", "AuditoriumId", c => c.Int());
            RenameColumn(table: "dbo.Lectures", name: "AuditoriumId", newName: "Auditorium_AuditoriumId");
            CreateIndex("dbo.Lectures", "Auditorium_AuditoriumId");
            AddForeignKey("dbo.Lectures", "Auditorium_AuditoriumId", "dbo.Auditoriums", "AuditoriumId");
        }
    }
}
