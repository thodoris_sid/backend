namespace AcademicCalendar.Infrastructure.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCoursesFields1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Courses", "CourseCode", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Courses", "CourseCode", c => c.Int(nullable: false));
        }
    }
}
