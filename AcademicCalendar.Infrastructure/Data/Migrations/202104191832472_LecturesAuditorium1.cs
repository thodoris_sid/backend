namespace AcademicCalendar.Infrastructure.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LecturesAuditorium1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Lectures", "AuditoriumId", "dbo.Auditoriums");
            DropIndex("dbo.Lectures", new[] { "AuditoriumId" });
            AlterColumn("dbo.Lectures", "AuditoriumId", c => c.Int());
            CreateIndex("dbo.Lectures", "AuditoriumId");
            AddForeignKey("dbo.Lectures", "AuditoriumId", "dbo.Auditoriums", "AuditoriumId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Lectures", "AuditoriumId", "dbo.Auditoriums");
            DropIndex("dbo.Lectures", new[] { "AuditoriumId" });
            AlterColumn("dbo.Lectures", "AuditoriumId", c => c.Int(nullable: false));
            CreateIndex("dbo.Lectures", "AuditoriumId");
            AddForeignKey("dbo.Lectures", "AuditoriumId", "dbo.Auditoriums", "AuditoriumId", cascadeDelete: true);
        }
    }
}
