namespace AcademicCalendar.Infrastructure.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AuditoriumAddFieldStudiesType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Auditoriums", "StudiesType", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Auditoriums", "StudiesType");
        }
    }
}
