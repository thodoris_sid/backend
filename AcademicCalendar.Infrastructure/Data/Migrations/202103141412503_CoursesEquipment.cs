namespace AcademicCalendar.Infrastructure.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CoursesEquipment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Equipments", "Course_CourseId", c => c.Int());
            CreateIndex("dbo.Equipments", "Course_CourseId");
            AddForeignKey("dbo.Equipments", "Course_CourseId", "dbo.Courses", "CourseId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Equipments", "Course_CourseId", "dbo.Courses");
            DropIndex("dbo.Equipments", new[] { "Course_CourseId" });
            DropColumn("dbo.Equipments", "Course_CourseId");
        }
    }
}
