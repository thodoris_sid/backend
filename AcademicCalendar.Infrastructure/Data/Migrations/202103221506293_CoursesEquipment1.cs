namespace AcademicCalendar.Infrastructure.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CoursesEquipment1 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Equipments", name: "AuditoriumId", newName: "Auditorium_AuditoriumId");
            RenameIndex(table: "dbo.Equipments", name: "IX_AuditoriumId", newName: "IX_Auditorium_AuditoriumId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Equipments", name: "IX_Auditorium_AuditoriumId", newName: "IX_AuditoriumId");
            RenameColumn(table: "dbo.Equipments", name: "Auditorium_AuditoriumId", newName: "AuditoriumId");
        }
    }
}
