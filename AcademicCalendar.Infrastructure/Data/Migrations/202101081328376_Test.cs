namespace AcademicCalendar.Infrastructure.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Test : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Lectures", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.Courses", "ProfessorId", "dbo.Professors");
            DropIndex("dbo.Courses", new[] { "ProfessorId" });
            AlterColumn("dbo.Courses", "ProfessorId", c => c.Int());
            CreateIndex("dbo.Courses", "ProfessorId");
            AddForeignKey("dbo.Lectures", "CourseId", "dbo.Courses", "CourseId");
            AddForeignKey("dbo.Courses", "ProfessorId", "dbo.Professors", "ProfessorId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Courses", "ProfessorId", "dbo.Professors");
            DropForeignKey("dbo.Lectures", "CourseId", "dbo.Courses");
            DropIndex("dbo.Courses", new[] { "ProfessorId" });
            AlterColumn("dbo.Courses", "ProfessorId", c => c.Int(nullable: false));
            CreateIndex("dbo.Courses", "ProfessorId");
            AddForeignKey("dbo.Courses", "ProfessorId", "dbo.Professors", "ProfessorId", cascadeDelete: true);
            AddForeignKey("dbo.Lectures", "CourseId", "dbo.Courses", "CourseId", cascadeDelete: true);
        }
    }
}
