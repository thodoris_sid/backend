namespace AcademicCalendar.Infrastructure.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LecturesAuditorium2 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Lectures", name: "AuditoriumId", newName: "Auditorium_AuditoriumId");
            RenameIndex(table: "dbo.Lectures", name: "IX_AuditoriumId", newName: "IX_Auditorium_AuditoriumId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Lectures", name: "IX_Auditorium_AuditoriumId", newName: "IX_AuditoriumId");
            RenameColumn(table: "dbo.Lectures", name: "Auditorium_AuditoriumId", newName: "AuditoriumId");
        }
    }
}
