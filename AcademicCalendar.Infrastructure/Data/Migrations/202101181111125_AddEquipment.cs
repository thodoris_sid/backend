namespace AcademicCalendar.Infrastructure.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEquipment : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Equipments",
                c => new
                    {
                        EquipmentId = c.Int(nullable: false, identity: true),
                        Type = c.String(),
                        TypeCount = c.Int(nullable: false),
                        AuditoriumId = c.Int(nullable: false),
                        Audit_CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Audit_UpdatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ExtraData = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.EquipmentId)
                .ForeignKey("dbo.Auditoriums", t => t.AuditoriumId, cascadeDelete: true)
                .Index(t => t.AuditoriumId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Equipments", "AuditoriumId", "dbo.Auditoriums");
            DropIndex("dbo.Equipments", new[] { "AuditoriumId" });
            DropTable("dbo.Equipments");
        }
    }
}
