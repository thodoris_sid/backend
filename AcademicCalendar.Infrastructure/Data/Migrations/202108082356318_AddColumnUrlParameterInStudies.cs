namespace AcademicCalendar.Infrastructure.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumnUrlParameterInStudies : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Studies", "UrlParameter", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Studies", "UrlParameter");
        }
    }
}
