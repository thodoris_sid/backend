namespace AcademicCalendar.Infrastructure.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addProfessorAuditoriumType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Auditoriums", "type", c => c.Short(nullable: false));
            AddColumn("dbo.Professors", "type", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Professors", "type");
            DropColumn("dbo.Auditoriums", "type");
        }
    }
}
