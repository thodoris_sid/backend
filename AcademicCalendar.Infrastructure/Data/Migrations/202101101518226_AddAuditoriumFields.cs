namespace AcademicCalendar.Infrastructure.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAuditoriumFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Auditoriums", "Name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Auditoriums", "Name");
        }
    }
}
