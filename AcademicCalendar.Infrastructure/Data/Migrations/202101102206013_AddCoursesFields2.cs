namespace AcademicCalendar.Infrastructure.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCoursesFields2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Courses", "Semester", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Courses", "Semester");
        }
    }
}
