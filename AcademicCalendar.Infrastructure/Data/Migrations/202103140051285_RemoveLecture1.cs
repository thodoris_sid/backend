namespace AcademicCalendar.Infrastructure.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveLecture1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Lectures", "AuditoriumId", "dbo.Auditoriums");
            DropIndex("dbo.Lectures", new[] { "AuditoriumId" });
            RenameColumn(table: "dbo.Lectures", name: "AuditoriumId", newName: "Auditorium_AuditoriumId");
            AlterColumn("dbo.Lectures", "Auditorium_AuditoriumId", c => c.Int());
            CreateIndex("dbo.Lectures", "Auditorium_AuditoriumId");
            AddForeignKey("dbo.Lectures", "Auditorium_AuditoriumId", "dbo.Auditoriums", "AuditoriumId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Lectures", "Auditorium_AuditoriumId", "dbo.Auditoriums");
            DropIndex("dbo.Lectures", new[] { "Auditorium_AuditoriumId" });
            AlterColumn("dbo.Lectures", "Auditorium_AuditoriumId", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.Lectures", name: "Auditorium_AuditoriumId", newName: "AuditoriumId");
            CreateIndex("dbo.Lectures", "AuditoriumId");
            AddForeignKey("dbo.Lectures", "AuditoriumId", "dbo.Auditoriums", "AuditoriumId", cascadeDelete: true);
        }
    }
}
