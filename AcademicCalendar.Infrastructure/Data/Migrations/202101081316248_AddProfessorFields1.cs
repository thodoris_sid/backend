namespace AcademicCalendar.Infrastructure.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProfessorFields1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Professors", "Description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Professors", "Description");
        }
    }
}
