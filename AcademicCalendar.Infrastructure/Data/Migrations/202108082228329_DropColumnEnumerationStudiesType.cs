namespace AcademicCalendar.Infrastructure.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DropColumnEnumerationStudiesType : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Courses", "StudiesType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Courses", "StudiesType", c => c.Short(nullable: false));
        }
    }
}
