// <auto-generated />
namespace AcademicCalendar.Infrastructure.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AddEquipment1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddEquipment1));
        
        string IMigrationMetadata.Id
        {
            get { return "202101181144395_AddEquipment1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
