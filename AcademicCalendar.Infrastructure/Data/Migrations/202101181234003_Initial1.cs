namespace AcademicCalendar.Infrastructure.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Equipments", "AuditoriumId", "dbo.Auditoriums");
            DropIndex("dbo.Equipments", new[] { "AuditoriumId" });
            AlterColumn("dbo.Equipments", "AuditoriumId", c => c.Int());
            CreateIndex("dbo.Equipments", "AuditoriumId");
            AddForeignKey("dbo.Equipments", "AuditoriumId", "dbo.Auditoriums", "AuditoriumId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Equipments", "AuditoriumId", "dbo.Auditoriums");
            DropIndex("dbo.Equipments", new[] { "AuditoriumId" });
            AlterColumn("dbo.Equipments", "AuditoriumId", c => c.Int(nullable: false));
            CreateIndex("dbo.Equipments", "AuditoriumId");
            AddForeignKey("dbo.Equipments", "AuditoriumId", "dbo.Auditoriums", "AuditoriumId", cascadeDelete: true);
        }
    }
}
