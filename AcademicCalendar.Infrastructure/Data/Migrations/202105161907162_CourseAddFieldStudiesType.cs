namespace AcademicCalendar.Infrastructure.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CourseAddFieldStudiesType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Courses", "StudiesType", c => c.Short(nullable: false));
            DropColumn("dbo.Auditoriums", "StudiesType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Auditoriums", "StudiesType", c => c.Short(nullable: false));
            DropColumn("dbo.Courses", "StudiesType");
        }
    }
}
