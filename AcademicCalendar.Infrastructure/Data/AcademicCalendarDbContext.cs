﻿using AcademicCalendar.Core.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Infrastructure.Data
{
    public class AcademicCalendarDbContext : DbContext
    {

        // Enable-Migrations -ContextTypeName  AcademicCalendar.Infrastructure.Data.AcademicCalendarDbContext -MigrationsDirectory Data\Migrations
        // Add-Migration -ConfigurationTypeName  AcademicCalendar.Infrastructure.Data.Migrations.Configuration Initial
        // Update-Database -ConfigurationTypeName  AcademicCalendar.Infrastructure.Data.Migrations.Configuration
        // Update-Database -ConfigurationTypeName AcademicCalendar.Infrastructure.Data.Migrations.Configuration -TargetMigration:0
        // rember to uncomment seed if you target migration 0
        public AcademicCalendarDbContext() : base("AcademicCalendarConnection")
        {
        }

        public AcademicCalendarDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Auditorium> Auditorium { get; set; }
        public DbSet<Equipment> Equipment { get; set; }
        public DbSet<Lecture> Lecture { get; set; }
        public DbSet<Course> Course { get; set; }
        public DbSet<Professor> Professor { get; set; }
        public DbSet<Studies> Studies { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            const string standardDateTimeType = "datetime2";
            modelBuilder.Properties<DateTime>()
             .Configure(p => p.HasColumnType(standardDateTimeType));

            modelBuilder.Entity<Lecture>()
                .HasRequired(l => l.Course)
                .WithMany(c => c.Lectures)
                .HasForeignKey(l => l.CourseId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Course>()
                .HasOptional(c => c.Professor)
                .WithMany(p => p.Courses)
                .HasForeignKey(c => c.ProfessorId);


        }

    }
}
