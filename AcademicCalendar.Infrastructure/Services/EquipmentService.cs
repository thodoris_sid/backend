﻿using AcademicCalendar.Core.Entities;
using AcademicCalendar.Infrastructure.Data;
using AcademicCalendar.Infrastructure.Interfaces;
using AcademicCalendar.Infrastructure.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Infrastructure.Services
{
    public class EquipmentService : IEquipmentService
    {
        private readonly AcademicCalendarDbContext _context;
        private readonly IMapper _mapper;

        public EquipmentService(AcademicCalendarDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task AddEquipment(EquipmentDto equipmentDto)
        {
            var equipment = _mapper.Map<Equipment>(equipmentDto);
            _context.Equipment.Add(equipment);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {

                throw e;
            }
        }

        public async Task<List<EquipmentDto>> GetAllEquipment()
        {
            var result = await _context.Equipment
                .AsNoTracking()
                //.Include(e => e.Auditorium)
                .Select(e => new EquipmentDto
                {
                    Type = e.Type,
                    TypeCount = e.TypeCount
                })
                .ToListAsync();

            return result;
        }

        public async Task<Equipment> GetEquipment(int id)
        {
            return null;
            //var result = await _context.Equipment.Include(c => c.Auditorium).FirstOrDefaultAsync(p => p.EquipmentId == id);
            //return result;
        }
    }
}
