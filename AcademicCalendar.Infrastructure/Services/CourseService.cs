﻿using AcademicCalendar.Core.Entities;
using AcademicCalendar.Core.Enumerations;
using AcademicCalendar.Infrastructure.Data;
using AcademicCalendar.Infrastructure.Exceptions;
using AcademicCalendar.Infrastructure.Interfaces;
using AcademicCalendar.Infrastructure.Models;
using AutoMapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Infrastructure.Services
{
    public class CourseService : ICourseService
    {
        private readonly AcademicCalendarDbContext _context;
        private readonly IMapper _mapper;
        public CourseService(AcademicCalendarDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task AddCourse(CourseDto courseDto)
        {
            var course = _mapper.Map<Course>(courseDto);
            course.ExtraData = JsonConvert.SerializeObject(courseDto.Equipment);
            _context.Course.Add(course);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {

                throw e;
            }
        }

        public async Task DeleteCourse(Course course)
        {
            course.IsDeleted = true;
            await _context.SaveChangesAsync();
        }

        public async Task<List<Course>> GetAllCourses(int studiesType)
        {
            var result = await _context.Course
                .AsNoTracking()
                .Where(a => a.IsDeleted == false)
                .Where(c => c.StudiesId == studiesType)
                .Include(c => c.Professor)
                .Include(c => c.Equipment)
                .ToListAsync();

            return result;
        }
        
        public async Task<List<CourseDto>> GetCoursesFromSemester(int semesterId)
        {
            var result = await _context.Course
                .AsNoTracking()
                .Where(a => a.IsDeleted == false)
                .Where(c => c.Semester == semesterId)
                .Include(c => c.Professor)
                .Select(c => new CourseDto { 
                    CourseId = c.CourseId,
                    Description = c.Description,
                    Title = c.Title,
                    CourseCode = c.CourseCode,
                    Semester = c.Semester,
                    IsInCalendar = c.IsInCalendar
                })
                .ToListAsync();

            return result;
        }

        public async Task<Course> GetCourse(int id)
        {
            var result = await _context.Course
                .Where(a => a.IsDeleted == false)
                .Include(c => c.Professor)
                .FirstOrDefaultAsync(p => p.CourseId == id);
            return result;
        }
    }
}
