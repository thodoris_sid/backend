﻿using AcademicCalendar.Core.Entities;
using AcademicCalendar.Core.Enumerations;
using AcademicCalendar.Infrastructure.Data;
using AcademicCalendar.Infrastructure.Interfaces;
using AcademicCalendar.Infrastructure.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Infrastructure.Services
{
    public class LectureService : ILectureService
    {
        private readonly AcademicCalendarDbContext _context;
        private readonly IMapper _mapper;

        public LectureService(AcademicCalendarDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task AddLecture(LectureDto lectureDto)
        {
            var lecture = _mapper.Map<Lecture>(lectureDto);
            _context.Lecture.Add(lecture);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {

                throw e;
            }
        }

        public async Task DeleteLecture(int courseId)
        {
           
            var course = await _context.Course.FirstOrDefaultAsync(c => c.CourseId == courseId);
            var lectures = await _context.Lecture
                .Where(l => l.CourseId == courseId)
                .ToListAsync();
            course.IsInCalendar = false;
            if (lectures != null)
            {
                _context.Lecture.RemoveRange(lectures);
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateException e)
                {

                    throw e;
                }
            }
        }

        public async Task DeleteLectures()
        {

            var lectures = await _context.Lecture.ToListAsync();
            if (lectures != null)
            {
                var courses = await _context.Course.Where(c=> c.IsInCalendar == true).ToListAsync();

                foreach (var course in courses) {
                    course.IsInCalendar = false;
                }

                _context.Lecture.RemoveRange(lectures);
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateException e)
                {

                    throw e;
                }
            }
        }


        public async Task<List<LectureDto>> GetAllLecturesByStudiesType(int studiesType)
        {
            var result = await _context.Lecture
                .AsNoTracking()
                .Include(l => l.Course)
                .Include(l => l.Auditorium)
                .Where(l=> l.Course.StudiesId == studiesType)
                .Select(l => new LectureDto {
                    LectureId = l.LectureId,
                    StartTime = l.StartTime,
                    EndTime = l.EndTime,
                    CourseId = l.CourseId,
                    StudiesId = (int)l.Course.StudiesId,
                    CourseName = l.Course.Title,
                    Semester = l.Course.Semester,
                    AuditoriumId = l.Auditorium.AuditoriumId,
                    ProfessorFirstName = l.Course.Professor.FirstName,
                    ProfessorLastName = l.Course.Professor.LastName,
                    ProfessorId = l.Course.Professor.ProfessorId
                })
                .ToListAsync();

            return result;
        }


        public async Task<List<LectureDto>> GetAllLectures()
        {
            var result = await _context.Lecture
                .AsNoTracking()
                .Include(l => l.Course)
                .Include(l => l.Auditorium)
                .Select(l => new LectureDto
                {
                    LectureId = l.LectureId,
                    StartTime = l.StartTime,
                    EndTime = l.EndTime,
                    CourseId = l.CourseId,
                    StudiesId = (int)l.Course.StudiesId,
                    CourseName = l.Course.Title,
                    Semester = l.Course.Semester,
                    AuditoriumId = l.Auditorium.AuditoriumId,
                    ProfessorFirstName = l.Course.Professor.FirstName,
                    ProfessorLastName = l.Course.Professor.LastName,
                    ProfessorId = l.Course.Professor.ProfessorId
                })
                .ToListAsync();

            return result;
        }

        public async Task<Lecture> GetLecture(int id)
        {
            var result = await _context.Lecture.FirstOrDefaultAsync(p => p.LectureId == id);
            if (result == null)
            {
                // TODO
            }
            return result;
        }
    }
}
