﻿using AcademicCalendar.Core.Entities;
using AcademicCalendar.Infrastructure.Data;
using AcademicCalendar.Infrastructure.Interfaces;
using AcademicCalendar.Infrastructure.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Infrastructure.Services
{
    public class ProfessorService : IProfessorService
    {

        private readonly AcademicCalendarDbContext _context;
        private readonly IMapper _mapper;

        public ProfessorService(AcademicCalendarDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task AddProfessor(ProfessorDto professorDto)
        {
            var professor = _mapper.Map<Professor>(professorDto);
            _context.Professor.Add(professor);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {

                throw e;
            }
        }

        public async Task UpdateProfessor(Professor professor, ProfessorDto professorDto)
        {

            professor.FirstName = professorDto.FirstName;
            professor.LastName = professorDto.LastName;
            professor.Phone = professorDto.Phone;
            professor.Email = professorDto.Email;
            professor.Website = professorDto.Website;
            professor.Office = professorDto.Office;
            professor.Description = professorDto.Description;
            professor.Type = professorDto.Type;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {

                throw e;
            }
        }

        public async Task<List<Professor>> GetAllProfessors(bool includeCourses = false)
        {

            if (includeCourses)
            {
                var resultIncludedCourses = await _context.Professor
                    .Where(a => a.IsDeleted == false)
                    .Include(p => p.Courses)
                    .AsNoTracking()
                    .ToListAsync();

                return resultIncludedCourses;
            }

            var result = await _context.Professor
            .AsNoTracking()
            .Where(a => a.IsDeleted == false)
            .ToListAsync();

            return result;
        }

        public async Task<Professor> GetProfessor(int id)
        {
            var result = await _context.Professor
                    .Where(a => a.IsDeleted == false)
                    .FirstOrDefaultAsync(p => p.ProfessorId == id);

            return result;
        }

        public async Task DeleteProfessor(Professor professor)
        {
            professor.IsDeleted = true;
            await _context.SaveChangesAsync();
        }

    }
}
