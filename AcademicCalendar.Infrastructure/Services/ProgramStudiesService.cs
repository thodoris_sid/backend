﻿using AcademicCalendar.Core.Entities;
using AcademicCalendar.Infrastructure.Data;
using AcademicCalendar.Infrastructure.Interfaces;
using AcademicCalendar.Infrastructure.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Infrastructure.Services
{
    public class ProgramStudiesService : IProgramStudiesService
    {

        private readonly AcademicCalendarDbContext _context;
        private readonly IMapper _mapper;

        public ProgramStudiesService(AcademicCalendarDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task AddProgramStudies(ProgramStudiesDto programStudiesDto)
        {

            var programStudies = _mapper.Map<Studies>(programStudiesDto);
            _context.Studies.Add(programStudies);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {

                throw e;
            }
        }

        public async Task<List<Studies>> GetProgramStudies()
        {
            var result = await _context.Studies
                .AsNoTracking()
                .Where(a => a.IsDeleted == false)
                .ToListAsync();

            return result;
        }

        public async Task<Studies> GetProgramStudies(int id)
        {
            var result = await _context.Studies
                .Where(a => a.IsDeleted == false)
                .FirstOrDefaultAsync(p => p.StudiesId == id);
            return result;
        }

        public async Task DeleteProgramStudies(Studies studies)
        {
            studies.IsDeleted = true;
            await _context.SaveChangesAsync();
        }

    }
}
