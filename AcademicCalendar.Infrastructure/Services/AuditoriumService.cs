﻿using AcademicCalendar.Core.Entities;
using AcademicCalendar.Infrastructure.Data;
using AcademicCalendar.Infrastructure.Interfaces;
using AcademicCalendar.Infrastructure.Models;
using AutoMapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Infrastructure.Services
{
    public class AuditoriumService : IAuditoriumService
    {

        private readonly AcademicCalendarDbContext _context;
        private readonly IMapper _mapper;

        public AuditoriumService(AcademicCalendarDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task AddAuditorium(AuditoriumDto auditoriumDto)
        {
            var auditorium = _mapper.Map<Auditorium>(auditoriumDto);
            auditorium.ExtraData = JsonConvert.SerializeObject(auditoriumDto.Equipment);
            _context.Auditorium.Add(auditorium);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {

                throw e;
            }
        }

        public async Task UpdateAuditorium(Auditorium auditorium, AuditoriumDto auditoriumDto)
        {

            auditorium.Name = auditoriumDto.Name;
            auditorium.Capacity = auditoriumDto.Capacity;
            auditorium.Type = auditoriumDto.Type;
            auditorium.ExtraData = JsonConvert.SerializeObject(auditoriumDto.Equipment);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {

                throw e;
            }
        }


        public async Task<List<Auditorium>> GetAllAuditoriums()
        {
            var result = await _context.Auditorium
                .AsNoTracking()
                .Where(a => a.IsDeleted == false)
                .Include(a => a.Equipment)
                .ToListAsync();

            return result;
        }

        public async Task<Auditorium> GetAuditorium(int id)
        {
            var result = await _context.Auditorium
                .Where(a => a.IsDeleted == false)
                .Include(a => a.Equipment)
                .FirstOrDefaultAsync(p => p.AuditoriumId == id);
            return result;
        }

        public async Task DeleteAuditorium(Auditorium auditorium)
        {
            auditorium.IsDeleted = true;
            await _context.SaveChangesAsync();
        }

    }
}
