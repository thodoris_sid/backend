﻿using AcademicCalendar.Core.Entities;
using AcademicCalendar.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Infrastructure.Models
{
    public class AuditoriumDto
    {
        public int AuditoriumId { get; set; }
        public string Name { get; set; }
        public int Capacity { get; set; }
        public AuditoriumType Type { get; set; }
        public ICollection<EquipmentDto> Equipment { get; set; }
    }
}
