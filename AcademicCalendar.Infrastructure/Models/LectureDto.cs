﻿using AcademicCalendar.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Infrastructure.Models
{
    public class LectureDto
    {
        public int LectureId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public CourseDto Course { get; set; }
        public AuditoriumDto Auditorium { get; set; }

        public int CourseId { get; set; }
        public int StudiesId { get; set; }

        public string CourseName { get; set; }
        public int Semester { get; set; }
        public int AuditoriumId { get; set; }
        public int ProfessorId { get; set; }
        public string ProfessorFirstName { get; set; }
        public string ProfessorLastName { get; set; }
    }
}
