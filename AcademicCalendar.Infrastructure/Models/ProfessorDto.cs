﻿using AcademicCalendar.Core.Entities;
using AcademicCalendar.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Infrastructure.Models
{
    public class ProfessorDto
    {
        public int ProfessorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Description { get; set; }
        public string Office { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public ProfessorType Type { get; set; }
        public ICollection<CourseDto> Courses { get; set; }
    }
}
