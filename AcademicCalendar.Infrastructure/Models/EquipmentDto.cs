﻿using AcademicCalendar.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Infrastructure.Models
{
    public class EquipmentDto
    {
        public int EquipmentId { get; set; }
        public string Type { get; set; }
        public int TypeCount { get; set; }

    }
}
