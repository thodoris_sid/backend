﻿using AcademicCalendar.Core.Entities;
using AcademicCalendar.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Infrastructure.Models
{
    public class CourseDto
    {
        public int CourseId { get; set; }
        public string CourseCode { get; set; }
        public int Semester { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsObligatory { get; set; }
        public ProfessorDto Professor { get; set; }
        public int ProfessorId { get; set; }
        public ICollection<EquipmentDto> Equipment { get; set; }
        public int StudiesId { get; set; }
        public bool IsInCalendar { get; set; }
    }
}
