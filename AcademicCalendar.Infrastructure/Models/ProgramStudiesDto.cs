﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Infrastructure.Models
{
    public class ProgramStudiesDto
    {
        public string Name { get; set; }
        public string UrlParameter { get; set; }
    }
}
