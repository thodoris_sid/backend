﻿using AcademicCalendar.Core.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Core.Entities
{
    public class Studies : BaseEntity
    {
        public int StudiesId { get; set; }
        public string Name { get; set; }
        public string UrlParameter { get; set; }
    }
}
