﻿using AcademicCalendar.Core.ValueObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Core.Entities.Base
{
    public class BaseEntity
    {

        public Audit Audit { get; set; }

        public string ExtraData { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime DeletedAt { get; set; }

        public BaseEntity()
        {
            Audit = new Audit();
            ExtraData = string.Empty;
        }

        public virtual void Update()
        {
            Audit.UpdatedAt = DateTime.UtcNow;
        }

        public virtual void Delete()
        {
            IsDeleted = true;
            DeletedAt = DateTime.UtcNow;
            Update();
        }
    }
}
