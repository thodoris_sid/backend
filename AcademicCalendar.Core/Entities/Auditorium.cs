﻿using AcademicCalendar.Core.Entities.Base;
using AcademicCalendar.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Core.Entities
{
    public class Auditorium : BaseEntity
    {
        public Auditorium()
        {
            Lectures = new List<Lecture>();
            Equipment = new List<Equipment>();
        }
        public int AuditoriumId { get; set; }
        public string Name { get; set; }
        public int Capacity { get; set; }
        public AuditoriumType Type { get; set; }
        public ICollection<Equipment> Equipment { get; set; }
        public ICollection<Lecture> Lectures { get; set; }
    }
}
