﻿using AcademicCalendar.Core.Entities.Base;
using AcademicCalendar.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Core.Entities
{
    public class Course : BaseEntity
    {

        public Course()
        {
            Lectures = new List<Lecture>();
            Equipment = new List<Equipment>();
        }

        public int CourseId { get; set; }
        public string CourseCode { get; set; }
        public int Semester { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsInCalendar { get; set; }
        public bool IsObligatory { get; set; }
        public Professor Professor { get; set; }
        public ICollection<Lecture> Lectures { get; set; }
        public ICollection<Equipment> Equipment { get; set; }
        public Studies Studies { get; set; }

        public int? ProfessorId { get; set; }
        public int? StudiesId { get; set; }
    }
}
