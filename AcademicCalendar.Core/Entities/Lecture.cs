﻿using AcademicCalendar.Core.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Core.Entities
{
    public class Lecture : BaseEntity
    {
        public int LectureId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public Auditorium Auditorium { get; set; }
        public Course Course { get; set; }
        public int CourseId { get; set; }
        public int AuditoriumId { get; set; }

    }
}
