﻿using AcademicCalendar.Core.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Core.Entities
{
    public class Equipment : BaseEntity
    {
        public int EquipmentId { get; set; }
        public string Type { get; set; }
        public int TypeCount { get; set; }

    }

}
