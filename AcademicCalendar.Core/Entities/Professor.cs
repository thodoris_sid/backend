﻿using AcademicCalendar.Core.Entities.Base;
using AcademicCalendar.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Core.Entities
{
    public class Professor : BaseEntity
    {
        public Professor()
        {
            Courses = new List<Course>();
        }

        public int ProfessorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Description { get; set; }
        public string Office { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public ProfessorType Type { get; set; }
        public ICollection<Course> Courses { get; set; }
    }
}
