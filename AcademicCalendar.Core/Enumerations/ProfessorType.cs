﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Core.Enumerations
{
    public enum ProfessorType : short
    {
        melhDep = 0,
        melhEdip = 10,
        exwterikoi = 20
    }
}
