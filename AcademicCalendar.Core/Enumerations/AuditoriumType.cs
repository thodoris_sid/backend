﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicCalendar.Core.Enumerations
{
    public enum AuditoriumType : short
    {
        aithousa = 0,
        ergasthrio = 10
    }
}
